﻿using System;

namespace Question02
{
    class Program
    {
        static void Main(string[] args)
        {
            //結果
            string[] result = null;

            //問題セット数
            string sqTurn;
            int qTurn = 0;
            //回答セット数
            string saTurn;
            int aTurn = 0;

            sqTurn = Console.ReadLine();
            qTurn = int.Parse(sqTurn);
            result = new string[qTurn];

            for (int i = 1; i <= qTurn; i++)
            {
                int resultFlg = 0;

                Random random = new Random();
                string sQuestion = random.Next(0, 9999).ToString("D4");
                char[] questionArr = sQuestion.ToCharArray();

                saTurn = Console.ReadLine();
                aTurn = int.Parse(saTurn);

                for (int j = 1; j <= aTurn; j++)
                {
                    int cntBulls = 0;
                    int cntCows = 0;
                    string sAnswer = Console.ReadLine();
                    char[] answerArr = sAnswer.ToCharArray();

                    for(int x =0; x < questionArr.Length; x++)
                    {
                        for (int y = 0; y < answerArr.Length; y++)
                        {
                            if (questionArr[x].Equals(answerArr[y]))
                            {
                                if (x == y)
                                {
                                    cntBulls++;
                                    resultFlg++;
                                }
                                else
                                {
                                    cntCows++;
                                }
                            }
                        }
                    }
                    Console.WriteLine("{0} {1}", cntBulls, cntCows);

                }
                if (resultFlg == 0)
                {
                    result[i - 1] = "None";
                }
                else
                {
                    result[i - 1] = sQuestion;
                }
            }

            foreach (string res in result)
            {
                Console.WriteLine("{0}", res);
            }
        }
    }
}
