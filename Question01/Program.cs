﻿using System;
using System.Collections;

namespace Question01
{
    class Program
    {
        static void Main(string[] args)
        {
            string sTurn, sSpecialAtt, sDataList;
            //入力の先頭行にはセット数
            int turn = 0;
            //必殺技使用可能回数
            int specialAtt = 0;
            //各入力データセット
            string[] dataList = null;
            //結果
            bool[] aResult = null;
            int[] aKill = null;

            sTurn = Console.ReadLine();
            turn = int.Parse(sTurn);


            aResult = new bool[turn];
            aKill = new int[turn];

            for (int i = 1; i <= turn; i++)
            {
                int kill = 0;
                bool result = true;

                sSpecialAtt = Console.ReadLine();
                specialAtt = int.Parse(sSpecialAtt);

                sDataList = Console.ReadLine();
                dataList = sDataList.Split(' ');

                foreach (string data in dataList)
                {
                    int iData = int.Parse(data);
                    if (iData <= 5)
                    {
                        //win
                        kill = kill + iData;
                    }
                    else if (iData > 5)
                    {
                        if (specialAtt == 0)
                        {
                            //loose
                            result = false;
                            break;
                        }
                        else if (specialAtt > 0)
                        {
                            //win
                            kill = kill + iData;
                            specialAtt--;
                        }
                    }
                }
                aKill[i - 1] = kill;
                aResult[i - 1] = result;
            }
            
            for (int j = 0; j <= turn-1; j++)
            {
                Console.WriteLine("{0} {1}", aResult[j], aKill[j]);
            }
        }
    }
}
